CREATE DATABASE summativedb;
USE summativedb;

CREATE TABLE student (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(20) NOT NULL,
surname VARCHAR(20) NOT NULL,
birthdate VARCHAR(20) NOT NULL,
gender VARCHAR(2) NOT NULL);

CREATE TABLE lesson (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(20) NOT NULL,
level VARCHAR(10) NOT NULL);

CREATE TABLE score (
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
student_id INT NOT NULL,
lesson_id INT NOT NULL,
score INT NOT NULL,
FOREIGN KEY (student_id) REFERENCES student(id),
FOREIGN KEY (lesson_id) REFERENCES lesson(id));

INSERT INTO student VALUES 
(NULL, "Toni Surya","Toni","18 Agustus 1999","L"),
(NULL, "Agung Putra","Aput","20 Desember 2000","L"),
(NULL, "Tania Ningsih","Taning","9 April 2001","P"),
(NULL, "Dewi Firda","Dewi","1 Februari 1999","P"),
(NULL, "Kusuma Putra","KusKus","6 Juni 1999","L");

INSERT INTO lesson VALUES 
(NULL,"Matematika","SMP"),
(NULL,"Bahasa Inggris","SMP"),
(NULL,"Bahasa Jepang","SMA"),
(NULL,"Penjaskes","SMA"),
(NULL,"TIK","SMA");

INSERT INTO score VALUES
(NULL,1,3,80),
(NULL,2,1,60),
(NULL,3,2,95),
(NULL,2,2,80),
(NULL,3,1,90),
(NULL,4,4,75),
(NULL,1,4,90),
(NULL,5,3,75),
(NULL,4,3,80),
(NULL,1,5,80),
(NULL,5,5,95),
(NULL,4,5,90),
(NULL,5,4,65);